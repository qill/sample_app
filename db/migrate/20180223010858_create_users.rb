class CreateUsers < ActiveRecord::Migration[5.1] 
  def change
    create_table :users do |t| # tはtableのt、
      t.string :name  # カラムの作成
      t.string :email

      t.timestamps # created_atとupdated_atというマジックカラムを作成
    end
  end
end
